NAME    = dp.tos
AS      = rmac
ASFLAGS = -p

ASM     = dp.s

# Build all
all: $(NAME)

# Generate target
$(NAME): $(ASM)
		$(AS) $< $(ASFLAGS) -o $@
		cp $@ ~/atari-fs/$@

# Remove all build files
clean:
		rm $(NAME)
